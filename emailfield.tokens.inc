<?php

/**
 * @file
 * Provides token integration for the emailfield module.
 *
 * Code shamelessly based on same code form the name module.
 * http://drupal.org/project/name
 */

/**
 * Implements hook_token_info().
 */
function emailfield_token_info($type = 'all') {
  $types = array(
    'emailfield' => array(
      'name' => t('Emailfield field values'),
      'description' => t('Emailfield field in the default format.'),
      'needs-data' => 'emailfield',
    ),
  );

  $tokens = array();
  $tokens['component-email'] = array(
    'name' => t('Component: Email'),
    'description' => t('The email address.'),
  );
  $tokens['component-email_typelabel'] = array(
    'name' => t('Component: Email type Label'),
    'description' => t('The email type label for this email.'),
  );
  $tokens['component-email_type'] = array(
    'name' => t('Component: Email type'),
    'description' => t('The email type id for the email type on this email.'),
  );

  $token_info = array(
    'types' => $types,
    'tokens' => array(
      'emailfield' => $tokens,
    ),
  );

  foreach (emailfield_token_types_chained(NULL, TRUE) as $entity_type => $tokens) {
    $token_info['tokens'][$entity_type] = $tokens;
  }

  return $token_info;
}

/**
 * Implements hook_tokens().
 */
function emailfield_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $sanitize = !empty($options['sanitize']);
  $replacements = array();

  // This handles the field tokens.
  if (isset($data[$type]) && $known_tokens = emailfield_token_types_chained($type)) {
    foreach ($tokens as $name => $original) {
      if (strpos($name, 'emailfield-') === 0) {
        /*
         * We handle a number of different combinations here.
         * token
         * token:[delta|all]
         * token:component-TYPE
         * token:[delta|all]:component-TYPE
         */
        $parts = explode(':', $name);
        $field_name = array_shift($parts);
        $field_name = str_replace('-', '_', substr($field_name, 6));

        // Ensure that this is actually a real field token before replacing.
        if ($field = field_info_field($field_name)) {
          $items = field_get_items($type, $data[$type], $field_name);
          if (empty($items)) {
            $replacements[$original] = '';
            continue;
          }

          // Find the delta value.
          $delta = NULL;
          $next = array_shift($parts);
          if (isset($next)) {
            if (is_numeric($next) && ((string) intval($next)) === (string) $next) {
              $delta = $next;
            }
            elseif ($next == 'all') {
              $delta = 'all';
            }
            else {
              // Return the value to the array for the next step.
              $delta = 0;
              array_unshift($parts, $next);
            }
          }
          else {
            $delta = 0;
          }

          if ($delta != 'all' && !isset($items[$delta])) {
            $replacements[$original] = '';
            continue;
          }

          // Find the token action and format / component.
          $action = NULL;
          $action_key = NULL;
          if ($next = array_shift($parts)) {
            if (strpos($next, 'component-') === 0) {
              $action = 'component';
              $action_key = substr($next, 10);
            }
          }
          else {
            $action_key = 'email';
            $action = 'component';
          }

          $formatted = array();
          if ($delta != 'all') {
            $items = array($items[$delta]);
          }
          foreach ($items as $item) {
            switch ($action_key) {
              case 'email_typelabel':
                if (isset($item['email_type'])) {
                  $entity_info = entity_get_info($type);
                  $key = $entity_info['bundle keys']['bundle'];
                  $instance = field_info_instance($type, $field['field_name'], $data[$type]->$key);
                  $allowed_values = emailfield_email_type_allowed_values($field, $instance, $type, $data[$type]);
                  if (isset($allowed_values[$item['email_type']])) {
                    $formatted[] = $allowed_values[$item['email_type']];
                  }
                }
                break;

              default:
                if (isset($item[$action_key])) {
                  $formatted[] = $item[$action_key];
                }
                break;
            }
          }
          $formatted = implode(', ', array_filter($formatted));
          $replacements[$original] = $sanitize ? check_plain($formatted) : $formatted;
        }
      }
    }
  }

  return $replacements;
}

/**
 * Defines a list of token types that can be chained with the emailfield field.
 *
 * @param mixed $type
 *   The type of token that we are working with. When NULL, works on
 *   all of them. Defaults to NULL.
 * @param bool $reset
 *   When TRUE, resets any cached data. Defaults to FALSE.
 *
 * @return array
 *   If an entity (token) type is given, returns the chained sub-list.
 */
function emailfield_token_types_chained($type = NULL, $reset = FALSE) {
  static $types = NULL;

  if (!isset($types) || $reset) {
    // Note that this hook contains translated strings, so each language is
    // cached separately.
    $langcode = $GLOBALS['language']->language;
    if (!$reset && $cache = cache_get("emailfield_token_types_chained:$langcode", 'cache')) {
      $types = $cache->data;
    }

    if (!$types) {
      $types = array();
      foreach (field_info_fields() as $field_name => $info) {
        if ($info['type'] == 'emailfield') {
          foreach ($info['bundles'] as $entity_type => $bundles) {
            $labels = array();
            foreach ($bundles as $bundle) {
              $instance = field_info_instance($entity_type, $field_name, $bundle);
              $labels[$instance['label']] = $instance['label'];
            }
            $label = array_shift($labels);
            $clean = str_replace('_', '-', $field_name);
            if (empty($labels)) {
              $description = t('Emailfield email address. To specify a delta value, use "@token:0". Append the other chained options after the delta value like this, "@token:0:component-email". Replace the delta value with all to obtain all items in the field like this "@token:all".',
                array('@token' => $clean));
            }
            else {
              $description = t('Emailfield email address. Also known as %labels', array('%labels' => implode(', ', $labels)));
            }
            $types[$entity_type]['emailfield-' . $clean] = array(
              'name' => check_plain($label),
              'description' => $description,
              'type' => 'emailfield',
            );
          }
        }
      }
      cache_set("emailfield_token_types_chained:$langcode", $types);
    }
  }

  if (isset($type)) {
    return isset($types[$type]) ? $types[$type] : NULL;
  }

  return $types;
}
