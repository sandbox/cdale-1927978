<?php
/**
 * @file
 * Provides devel generate integration.
 */

/**
 * Implements hook_devel_generate().
 */
function emailfield_devel_generate($object, $field, $instance, $bundle) {
  if (field_behaviors_widget('multiple values', $instance) == FIELD_BEHAVIOR_CUSTOM) {
    return devel_generate_multiple('_emailfield_devel_generate', $object, $field, $instance, $bundle);
  }
  else {
    return _emailfield_devel_generate($object, $field, $instance, $bundle);
  }
}

/**
 * Devel generate callback for emailfield field types.
 */
function _emailfield_devel_generate($object, $field, $instance, $bundle) {
  $email_type = '';
  $settings = $instance['widget']['settings'];
  if ($settings['enable_email_type']) {
    $allowed_values = emailfield_email_type_allowed_values($field, $instance) + array('' => '');
    $email_type = array_rand($allowed_values);
  }

  $tlds = array_flip(array(
    '.com', '.co', '.info', '.net', '.org', '.me', '.mobi', '.us', '.biz',
    '.tv', '.ca', '.com.au', '.net.au', '.org.au', '.mx', '.ws', '.ag',
    '.com.ag', '.net.ag', '.org.ag', '.am', '.asia', '.at', '.be', '.com.br',
    '.net.br', '.bz', '.com.bz', '.net.bz', '.cc', '.com.co', '.net.co',
    '.nom.co', '.de', '.es', '.com.es', '.nom.es', '.org.es', '.eu', '.fm',
    '.fr', '.gs', '.in', '.co.in', '.firm.in', '.gen.in', '.ind.in', '.net.in',
    '.org.in', '.it', '.jobs', '.jp', '.ms', '.com.mx', '.nl', '.nu', '.co.nz',
    '.net.nz', '.org.nz', '.se', '.tk', '.tw', '.com.tw', '.idv.tw', '.org.tw',
    '.co.uk', '.me.uk', '.org.uk', '.vg', '.xxx',
  ));

  $email = strtolower(str_replace(' ', '@', devel_create_greeking(2, TRUE))) . array_rand($tlds);

  return array(
    'email' => $email,
    'email_type' => $email_type,
  );
}
