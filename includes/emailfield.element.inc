<?php

/**
 * @file
 * Provides FAPI implementation for an emailfield element.
 */

/**
 * Implements hook_element_info().
 */
function _emailfield_element_info() {
  $path = drupal_get_path('module', 'emailfield');

  $types['emailfield_field'] = array(
    '#input' => TRUE,
    '#process' => array('ajax_process_form', 'emailfield_element_process'),
    '#element_validate' => array('emailfield_element_validate'),
    '#theme_wrappers' => array('form_element'),
    '#attached' => array(
      'css' => array($path . '/theme/emailfield.css'),
    ),
    '#emailfield_settings' => array(
      'bubble_errors' => FALSE,
      'use_email_input' => TRUE,
      'size' => 60,
      'enable_email_type' => TRUE,
      'email_type_allowed_values' => array(
        'work' => t('Work'),
        'personal' => t('Personal'),
      ),
      'email_type_allowed_values_position' => 'before',
    ),
  );

  $types['emailfield_email'] = array(
    '#input' => TRUE,
    '#size' => 30,
    '#maxlength' => 128,
    '#autocomplete_path' => FALSE,
    '#process' => array('ajax_process_form'),
    '#theme' => 'emailfield_email',
    '#theme_wrappers' => array('form_element'),
  );

  return $types;
}

/**
 * Process an individual emailfield element.
 */
function emailfield_element_process($element, &$form_state, $form) {
  $item = $element['#value'];

  $settings = $element['#emailfield_settings'];

  if ($settings['enable_email_type'] && !empty($settings['email_type_allowed_values'])) {
    $element['email_type'] = array(
      '#type' => 'select',
      '#options' => $settings['email_type_allowed_values'],
      '#weight' => $settings['email_type_allowed_values_position'] == 'after' ? 5 : -5,
      '#empty_option' => t('- Select -'),
      '#default_value' => isset($item['email_type']) ? $item['email_type'] : NULL,
    );
  }
  else {
    $element['email_type'] = array(
      '#type' => 'hidden',
      '#value' => isset($item['email_type']) ? $item['email_type'] : NULL,
    );
  }

  $element['email'] = array(
    '#type' => !empty($settings['use_email_input']) ? 'emailfield_email' : 'textfield',
    '#maxlength' => $settings['size'],
    '#size' => $settings['size'],
    '#required' => ($element['#delta'] == 0 && $element['#required']) ? $element['#required'] : FALSE,
    '#default_value' => isset($item['email']) ? $item['email'] : NULL,
    '#weight' => 0,
  );

  return $element;
}

/**
 * An #element_validate callback for the emailfield element.
 */
function emailfield_element_validate(&$element, &$form_state) {
  $item = $element['#value'];
  if (isset($item['email'])) {
    $email_input = trim($item['email']);
  }

  if (isset($email_input) && !empty($email_input)) {
    if (!valid_email_address($email_input)) {
      // If this is used in a field widget, bubble the errors to be handled
      // by hook_field_validate(). We can set a more useful message there
      // as we'll have full access to the field information, and won't have
      // to write complex code here to handle the case when this is used
      // as it's own FAPI element outside of the field system.
      if (isset($settings['bubble_errors']) && $settings['bubble_errors'] === TRUE) {
        $dummy_element = array(
          '#parents' => array_merge($element['#parents'], array('error')),
        );
        form_set_value($dummy_element, TRUE, $form_state);
      }
      else {
        form_error($element, t('%name: "%email" is not a valid email address.', array('%name' => $element['#title'], '%email' => $email_input)));
      }
    }
  }
}

/**
 * Returns HTML for a email form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #description, #size, #maxlength,
 *     #placeholder, #required, #attributes, #autocomplete_path.
 *
 * @ingroup themeable
 */
function theme_emailfield_email($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'email';
  element_set_attributes($element, array(
    'id', 'name', 'value', 'size',
    'maxlength', 'placeholder',
  ));
  _form_set_class($element, array('form-emailfield-email'));
  _form_set_class($element, array('form-text'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}
