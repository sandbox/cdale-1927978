<?php

/**
 * @file
 * Support for migrate module.
 */

/**
 * Implements hook_migrate_api().
 */
function emailfield_migrate_api() {
  return array(
    'api' => 2,
    'field handlers' => array('MigrateEmailfieldFieldHandler'),
  );
}

/**
 * @file
 * Support for the Migrate package.
 */
class MigrateEmailfieldFieldHandler extends MigrateFieldHandler {

  public function __construct() {
    $this->registerTypes(array('emailfield'));
  }

  /**
   * Arguments for an emailfield field migration.
   *
   * @param string $email_type
   *   The email type for an email.
   * @param string $language
   *   Language of the text (defaults to destination language).
   *
   * @return array
   *   An array of all the arguments.
   */
  static function arguments($email_type = NULL, $language = NULL) {
    $arguments = array();
    foreach (array('email_type', 'language') as $field) {
      if (isset($$field)) {
        $arguments[$field] = $$field;
      }
    }

    return $arguments;
  }

  /**
   * Implementation of MigrateFieldHandler::fields().
   *
   * @param string $type
   *   The field type.
   * @param string $parent_field
   *   Name of the parent field.
   * @param Migration $migration
   *   The migration context for the parent field. We can look at the mappings
   *   and determine which subfields are relevant.
   *
   * @return array
   *   The array of subfields we support.
   */
  public function fields($type, $parent_field, $migration = NULL) {
    $fields = array(
      'email_type' => t('Subfield: The email email_type'),
      'language' => t('Subfield: Language for the field'),
    );

    return $fields;
  }

  /**
   * Converts incoming data into the proper field arrays for Email fields.
   *
   * @param object $entity
   *   The destination entity which will hold the field arrays.
   * @param array $field_info
   *   Metadata for the date field being populated.
   * @param array $instance
   *   Metadata for this instance of the date field being populated.
   * @param array $values
   *   Array of date values to be fielded.
   *
   * @return array|null
   *   An array of date fields.
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }

    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    // Setup the standard Field API array for saving.
    $delta = 0;
    foreach ($values as $email) {
      $item = array();
      if (isset($arguments['email_type'])) {
        if (is_array($arguments['email_type'])) {
          $item['email_type'] = $arguments['email_type'][$delta];
        }
        else {
          $item['email_type'] = $arguments['email_type'];
        }
      }

      $item['email'] = $email;

      if (is_array($language)) {
        $current_language = $language[$delta];
      }
      else {
        $current_language = $language;
      }
      $return[$current_language][] = $item;
      $delta++;
    }

    return isset($return) ? $return : NULL;
  }
}
