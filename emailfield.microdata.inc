<?php

/**
 * @file
 * Provides microdata integration for the emailfield module.
 */

/**
 * Implements hook_microdata_suggestions().
 */
function emailfield_microdata_suggestions() {
  $suggestions = array(
    'fields' => array(
      'emailfield' => array(
        'schema.org' => array(
          '#itemprop' => array('email'),
        ),
      ),
    ),
  );

  return $suggestions;
}
